package pl.edu.agh.szubertm.symulatorewolucyjny.logic;

import pl.edu.agh.szubertm.symulatorewolucyjny.logic.geometric.Vector2d;
import pl.edu.agh.szubertm.symulatorewolucyjny.logic.mapelement.MapElement;

public interface IPositionChangeObserver {
    void positionChanged(IPositionChangePublisher source, MapElement movedElement, Vector2d oldPosition, Vector2d newPosition);
}
