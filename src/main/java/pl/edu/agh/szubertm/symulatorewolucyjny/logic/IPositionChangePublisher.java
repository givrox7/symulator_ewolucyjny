package pl.edu.agh.szubertm.symulatorewolucyjny.logic;

public interface IPositionChangePublisher {
    void addObserver(IPositionChangeObserver observer);
    void removeObserver(IPositionChangeObserver observer);
}
